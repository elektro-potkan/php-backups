Backups
=======

Manages backups for PHP projects.
This repo holds main interfaces, basic Managers, Jobs and PurgeRules.


Usage
-----
The package is built around the idea of backup manager,
to which all backup jobs are registered.
The manager also purges older backups using the purge rules,
keeping the storage used for backups under control.

### Example
```php
// init
$purgeRule = new ElektroPotkan\Backups\PurgeRules\Rolling;

$manager = new ElektroPotkan\Backups\Managers\Locking('path-to-backups-directory', 'MyApp-v1.47.2', $purgeRule);

$manager->addJob('config', new ElektroPotkan\Backups\Jobs\FileCopy('config/main.cfg'));
$manager->addJob('readme', new ElektroPotkan\Backups\Jobs\File('txt', 'Some README notes to keep in the backup for future restore'));
$manager->addJob('data', new ElektroPotkan\Backups\Jobs\Callback('json',
	function(string $path) use($jsonSource): void {
		if($jsonSource->hasData()){
			file_put_contents($path, $jsonSource->getData());
		}
	}
));

// run periodically
$manager->create();
$manager->purge();

// backend management
$manager->list();
$manager->get($id);
$manager->delete($id);

// backup details
$backup = $manager->list()[0];
$backup->id;
$backup->time;
$backup->size;

$file = $backup->files[0];
$file->name;
$file->path;
$file->size;
```

### Custom backup job
```php
class MyJob implements ElektroPotkan\Backups\IJob {
	private $api;
	
	
	public function __construct($api){
		$this->api = $api;
	}
	
	public function create(string $path): void {
		if($this->api->hasData()){
			$this->api->saveData($path);
		}
	}
	
	public function getExtension(): string {
		return 'data';
	}
}


// register the new job
$myJob = new MyJob($myApi);

$manager->addJob('api', $myJob);
```

### Custom purge rule
```php
class MyRule implements ElektroPotkan\Backups\IPurgeRule {
	public function keepOrPurge(DateTimeInterface $dt, DateTimeInterface $now): bool {
		// keep backups from last 12 days and from January 1 of each year
		return $dt >= Nette\Utils\DateTime::from($now)->modify('- 12 days')->setTime(0, 0, 0, 0) || $dt->format('z') === '0';
	}
}

$purgeRule = new MyRule;
```

### Job file-name
The jobs files are named using the schema:
```
timestamp_date_name_job-name.ext
```
E.g. for manager configured with `MyApp-v1.47.2` as `$name` parameter,
backup created at `2021-08-30 20:07:24 UTC`,
job added with the name `test` and returning extension `txt`,
the resulting file-name would be:
```
1630354044_2021-08-30_MyApp-v1.47.2_test.txt
```

### Logging
To log info and errors from built-in managers,
You need to provide an implementation of `ElektroPotkan\Backups\ILogger`
interface via `setLogger` call:
```php
// $logger implements ElektroPotkan\Backups\ILogger
$manager->setLogger($logger);
```

If You are using `Tracy`, You can use the provided `TracyLogger`:
```php
// $logger implements Tracy\ILogger
$manager->setLogger(new ElektroPotkan\Backups\TracyLogger($logger));
```


Author
------
Elektro-potkan <git@elektro-potkan.cz>


Info
----
### Versioning
This project uses [Semantic Versioning 2.0.0 (semver.org)](https://semver.org).

### Branching
This project uses slightly modified Git-Flow Workflow and Branching Model:
- https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow
- https://nvie.com/posts/a-successful-git-branching-model/


License
-------
You may use this program under the terms of either the BSD Zero Clause License or the GNU General Public License (GPL) version 3 or later.

See file [LICENSE](LICENSE.md).
