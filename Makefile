.PHONY: clean phpstan tests coverage-html coverage-xml


TMPDIR=/tmp/php-packages-tests/elektro-potkan/backups


all: phpstan tests

clean:
	-rm tests/phpstan.local.neon
	-rm -r '$(TMPDIR)'


$(TMPDIR):
	mkdir -p '$@'

tests/phpstan.local.neon:
	echo 'includes:' > '$@'
	echo '	- phpstan.neon' >> '$@'
	echo 'parameters:' >> '$@'
	echo '	tmpDir: $(TMPDIR)/phpstan' >> '$@'


phpstan: $(TMPDIR) tests/phpstan.local.neon
	vendor/bin/phpstan analyse -c tests/phpstan.local.neon -l max src

tests: $(TMPDIR)
	vendor/bin/tester -p php -c tests/php.ini --temp '$(TMPDIR)' tests/cases

coverage-html: $(TMPDIR)
	vendor/bin/tester -p phpdbg -c tests/php.ini --temp '$(TMPDIR)' --coverage '$(TMPDIR)/coverage.html' --coverage-src src tests/cases

coverage-xml: $(TMPDIR)
	vendor/bin/tester -p phpdbg -c tests/php.ini --temp '$(TMPDIR)' --coverage '$(TMPDIR)/coverage.xml' --coverage-src src tests/cases
