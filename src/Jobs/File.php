<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\Jobs;

use InvalidArgumentException;
use Nette;

use ElektroPotkan\Backups\IJob;


/**
 * Backup job storing given content
 */
class File implements IJob {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $ext;
	
	/** @var ?string */
	private $content = null;
	
	
	/**
	 * Constructor
	 * @param string $ext - backup file extension to request (can be empty)
	 */
	public function __construct(string $ext, ?string $content = null){
		$this->ext = $ext;
		$this->content = $content;
	} // constructor
	
	/**
	 * Sets file content to store
	 * If no content set, no backup file will be created.
	 * @param ?string $content - set to null to clear previously set content
	 */
	public function setContent(?string $content): void {
		$this->content = $content;
	} // setContent
	
	/**
	 * Creates backup file
	 * @param string $path - exact full path to file to create
	 */
	public function create(string $path): void {
		if($this->content !== null){
			file_put_contents($path, $this->content);
		};
	} // create
	
	/**
	 * Returns file extension
	 * @return string - extension of output backup file
	 */
	public function getExtension(): string {
		return $this->ext;
	} // getExtension
} // class File
