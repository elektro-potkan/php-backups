<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\Jobs;

use Nette;

use ElektroPotkan\Backups\IJob;


class Callback implements IJob {
	use Nette\SmartObject;
	
	
	/** @var callable(string): void */
	private $callback;
	
	/** @var string */
	private $ext;
	
	
	/**
	 * Constructor
	 * @param callable(string): void $callback - function to call to create a backup file (file path will be passed as first argument)
	 * @param string $ext - backup file extension
	 */
	public function __construct(string $ext, callable $callback){
		$this->callback = $callback;
		$this->ext = $ext;
	} // constructor
	
	/**
	 * Creates backup file
	 * @param string $path - exact full path to file to create
	 */
	public function create(string $path): void {
		call_user_func($this->callback, $path);
	} // create
	
	/**
	 * Returns file extension
	 * @return string - extension of output backup file
	 */
	public function getExtension(): string {
		return $this->ext;
	} // getExtension
} // class Callback
