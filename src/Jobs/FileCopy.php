<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\Jobs;

use InvalidArgumentException;
use Nette;
use Nette\Utils\FileSystem;

use ElektroPotkan\Backups\IJob;


/**
 * Backup job copying given file into backup
 */
class FileCopy implements IJob {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $path;
	
	/** @var ?string - also acts as cache when initially null */
	private $ext;
	
	
	/**
	 * Constructor
	 * @param string $path - full path to file that should be copied into backup (if exist when job run)
	 * @param ?string $ext - backup file extension (will be retrieved from $path if null)
	 */
	public function __construct(string $path, ?string $ext = null){
		$this->path = $path;
		$this->ext = $ext;
	} // constructor
	
	/**
	 * Creates backup file
	 * @param string $path - exact full path to file to create
	 */
	public function create(string $path): void {
		if(is_file($this->path)){
			FileSystem::copy($this->path, $path);
		};
	} // create
	
	/**
	 * Returns file extension
	 * @return string - extension of output backup file
	 */
	public function getExtension(): string {
		if($this->ext === null){
			$pi = pathinfo($this->path);
			
			if($pi['filename'] === '' && $pi['basename'][0] === '.'){// dot-file with no extension
				$this->ext = '';
			}
			else {
				$this->ext = $pi['extension'] ?? '';
			};
		};
		
		return $this->ext;
	} // getExtension
} // class FileCopy
