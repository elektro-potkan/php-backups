<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;


/**
 * Backup Job interface
 */
interface IJob {
	/**
	 * Creates backup file
	 *
	 * If there is no data, the job might not create any file.
	 * This is not considered an error.
	 *
	 * @param string $path - exact full path to file to create
	 */
	function create(string $path): void;
	
	/**
	 * Returns file extension (can be empty)
	 *
	 * States a job's request for the manager which should generally obey it.
	 * However, it is not guaranteed, that the path passed to create method
	 * will have the same extension as returned by this method.
	 * If the job somehow internally depends on the exact path extension given,
	 * it should check it in the create method.
	 *
	 * @return string - extension of output backup file
	 */
	function getExtension(): string;
} // interface IJob
