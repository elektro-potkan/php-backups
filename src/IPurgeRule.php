<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;

use DateTimeInterface;


/**
 * Purge rule interface
 */
interface IPurgeRule {
	/**
	 * Returns whether backup with given timestamp should be kept or purged
	 * @return bool - true to keep backup, false to delete it
	 */
	function keepOrPurge(DateTimeInterface $timestamp, DateTimeInterface $now): bool;
} // interface IPurgeRule
