<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;

use Nette;
use Nette\FileNotFoundException;
use SplFileInfo;


/**
 * Single backup file info
 *
 * @property-read string $name
 * @property-read string $path
 * @property-read int $size
 */
class BackupFile implements IBackupFile {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $name;
	
	/** @var string */
	private $path;
	
	/** @var int */
	private $size;
	
	
	/**
	 * Constructor
	 */
	public function __construct(SplFileInfo $file){
		$size = $file->getSize();
		if($size === false){// @phpstan-ignore-line
			throw new FileNotFoundException('Error when querying file referenced by given SplFileInfo instance!');
		};
		
		$this->name = $file->getFilename();
		$this->path = $file->getPathname();
		$this->size = $size;
	} // constructor
	
	/**
	 * Returns file-name of backup file
	 */
	public function getName(): string {
		return $this->name;
	} // getName
	
	/**
	 * Returns full path to backup file
	 */
	public function getPath(): string {
		return $this->path;
	} // getPath
	
	/**
	 * Returns size of backup file
	 */
	public function getSize(): int {
		return $this->size;
	} // getSize
} // class BackupFile
