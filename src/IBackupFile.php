<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;


/**
 * Single backup file interface
 */
interface IBackupFile {
	/**
	 * Returns file-name of backup file
	 */
	function getName(): string;
	
	/**
	 * Returns full path to backup file
	 */
	function getPath(): string;
	
	/**
	 * Returns size of backup file
	 */
	function getSize(): int;
} // interface IBackupFile
