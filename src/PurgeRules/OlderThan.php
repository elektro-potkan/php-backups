<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\PurgeRules;

use DateTimeInterface;
use Nette;

use ElektroPotkan\Backups\IPurgeRule;


/**
 * Simple time threshold purge rule
 *
 * Keeps backups newer than specific time.
 */
class OlderThan implements IPurgeRule {
	use Nette\SmartObject;
	
	
	/** @var DateTimeInterface */
	private $time;
	
	
	/**
	 * Constructor
	 */
	public function __construct(DateTimeInterface $time){
		$this->time = $time;
	} // constructor
	
	/**
	 * Returns whether backup with given timestamp should be kept or purged
	 * @return bool - true to keep backup, false to delete it
	 */
	public function keepOrPurge(DateTimeInterface $dt, DateTimeInterface $now): bool {
		return ($dt >= $this->time);
	} // keepOrPurge
} // class OlderThan
