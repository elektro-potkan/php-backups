<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\PurgeRules;

use DateTimeInterface;
use InvalidArgumentException;
use Nette;
use Nette\Utils\DateTime;

use ElektroPotkan\Backups\IPurgeRule;


/**
 * Last n minutes purge rule
 */
class Recent implements IPurgeRule {
	use Nette\SmartObject;
	
	
	/** @var int */
	private $mins;
	
	
	/**
	 * Constructor
	 * @param int $mins - age threshold in minutes
	 */
	public function __construct(int $mins){
		if($mins < 1){
			throw new InvalidArgumentException('Number of last minutes must be a positive integer!');
		};
		
		$this->mins = $mins;
	} // constructor
	
	/**
	 * Returns whether backup with given timestamp should be kept or purged
	 * @return bool - true to keep backup, false to delete it
	 */
	public function keepOrPurge(DateTimeInterface $dt, DateTimeInterface $now): bool {
		return ($dt >= DateTime::from($now)->modify(-$this->mins . ' minutes'));
	} // keepOrPurge
} // class Recent
