<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\PurgeRules;

use DateTimeInterface;
use Nette;

use ElektroPotkan\Backups\IPurgeRule;


/**
 * Stub to keep all files effectively disabling purging at all
 */
class AlwaysKeep implements IPurgeRule {
	use Nette\SmartObject;
	
	
	/**
	 * Returns whether backup with given timestamp should be kept or purged
	 * @return bool - true to keep backup, false to delete it
	 */
	public function keepOrPurge(DateTimeInterface $dt, DateTimeInterface $now): bool {
		return true;
	} // keepOrPurge
} // class AlwaysKeep
