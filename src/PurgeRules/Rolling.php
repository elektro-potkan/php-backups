<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\PurgeRules;

use DateTimeInterface;
use InvalidArgumentException;
use Nette;
use Nette\Utils\DateTime;

use ElektroPotkan\Backups\IPurgeRule;


/**
 * Rolling purge rule
 *
 * Keeps backups from:
 *   - 1st day of each year
 *   - 1st day of each month in current year
 *   - Monday of each week in current month
 *   - all backups from last n days (7 by default)
 */
class Rolling implements IPurgeRule {
	use Nette\SmartObject;
	
	
	/** @var int */
	private $days;
	
	
	/**
	 * Constructor
	 * @param int $days - number of last days to keep (at least 1 day, default 7 days)
	 */
	public function __construct(int $days = 7){
		if($days < 1){
			throw new InvalidArgumentException('Number of last days must be a positive integer!');
		};
		
		$this->days = $days;
	} // constructor
	
	/**
	 * Returns whether backup with given timestamp should be kept or purged
	 * @return bool - true to keep backup, false to delete it
	 */
	public function keepOrPurge(DateTimeInterface $dt, DateTimeInterface $now): bool {
		return (
				($dt->format('z') === '0')// 1st day of year
			||
				($dt->format('Y') === $now->format('Y') && (// current year
						($dt->format('j') === '1')// 1st day of month
					||
						($dt->format('n') === $now->format('n') && (// current month
								($dt->format('w') === '1')// Monday
							||
								($dt >= DateTime::from($now)->modify(-$this->days . ' days')->setTime(0, 0, 0, 0))// last n days
						))
				))
		);
	} // keepOrPurge
} // class Rolling
