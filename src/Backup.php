<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;

use DateTimeImmutable;
use DateTimeInterface;
use Nette;
use Nette\InvalidStateException;


/**
 * Single backup
 *
 * @property-read int $id
 * @property-read IBackupFile $mainFile
 * @property-read IBackupFile[] $files
 * @property-read int $size
 * @property-read DateTimeInterface $time
 */
class Backup implements IBackup {
	use Nette\SmartObject;
	
	
	/** @var int */
	private $id;
	
	/** @var DateTimeInterface */
	private $time;
	
	/** @var IBackupFile */
	private $mainFile = null;
	
	/** @var IBackupFile[] */
	private $files = [];
	
	/** @var bool */
	private $filesSorted = true;
	
	
	/**
	 * Constructor
	 */
	public function __construct(int $id, DateTimeInterface $time){
		$this->id = $id;
		//$this->time = DateTimeImmutable::createFromInterface($time);// available in PHP 8
		$this->time = new DateTimeImmutable($time->format('Y-m-d H:i:s.u'), $time->getTimezone());
	} // constructor
	
	/**
	 * Sets main backup file (e.g. an archive of whole backup)
	 *
	 * Do not call addFile using the same file - there is no check for duplicates.
	 * Each file should be registered into Backup only once
	 * (via setMainFile or addFile but not both).
	 */
	public function setMainFile(IBackupFile $file): void {
		if($this->mainFile !== null){
			throw new InvalidStateException('Main file already set!');
		};
		
		$this->mainFile = $file;
	} // setMainFile
	
	/**
	 * Adds backup file
	 */
	public function addFile(IBackupFile $file): void {
		$this->files[] = $file;
		$this->filesSorted = false;
	} // addFile
	
	/**
	 * Returns backup ID
	 */
	public function getId(): int {
		return $this->id;
	} // getId
	
	/**
	 * Returns backup creation time
	 */
	public function getTime(): DateTimeInterface {
		return $this->time;
	} // getTime
	
	/**
	 * Returns main file of the backup (e.g. an archive of all other files)
	 * @return null if no such file exists
	 */
	public function getMainFile(): ?IBackupFile {
		return $this->mainFile;
	} // getMainFile
	
	/**
	 * Returns all backup files
	 * @return IBackupFile[]
	 */
	public function getFiles(): array {
		$this->sortFiles();
		return $this->files;
	} // getFiles
	
	/**
	 * Returns size of whole backup (all its files)
	 */
	public function getSize(): int {
		$size = ($this->mainFile !== null) ? $this->mainFile->getSize() : 0;
		
		foreach($this->files as $file){
			$size += $file->getSize();
		};
		
		return $size;
	} // getSize
	
	/**
	 * Sorts backup files array by file-name
	 */
	protected function sortFiles(bool $force = false): void {
		if(count($this->files) < 2){
			return;// no or single file - nothing to do
		};
		
		if(!$force && $this->filesSorted){
			return;// already sorted - nothing to do
		};
		
		$names = [];
		foreach($this->files as $i => $file){
			$names[$i] = $file->getName();
		};
		
		array_multisort($names, $this->files);
		
		$this->filesSorted = true;
	} // sortFiles
} // class Backup
