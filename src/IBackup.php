<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;

use DateTimeInterface;


/**
 * Backup interface
 */
interface IBackup {
	/**
	 * Returns backup ID
	 */
	function getId(): int;
	
	/**
	 * Returns all backup files
	 * @return IBackupFile[]
	 */
	function getFiles(): array;
	
	/**
	 * Returns main file of the backup (e.g. an archive of all other files)
	 *
	 * Returned file should not be contained in the getFiles() result.
	 *
	 * @return null if no such file exists (or feature not supported by the IBackup instance creator)
	 */
	function getMainFile(): ?IBackupFile;
	
	/**
	 * Returns size of whole backup (all its files)
	 */
	function getSize(): int;
	
	/**
	 * Returns backup creation time
	 */
	function getTime(): DateTimeInterface;
} // interface IBackup
