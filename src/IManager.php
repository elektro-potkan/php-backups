<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;


/**
 * Backups manager interface
 */
interface IManager {
	/**
	 * Adds backup job
	 * @param string $name - unique name of the job, will be used as part of its backup file-name
	 */
	function addJob(string $name, IJob $job): void;
	
	/**
	 * Creates new backup
	 * @return int - ID of created backup
	 */
	function create(): int;
	
	/**
	 * Deletes backup of given ID
	 * @return bool
	 *   - true = OK
	 *   - false = ERROR
	 *   - null = not found
	 */
	function delete(int $id): ?bool;
	
	/**
	 * Returns backup of given ID
	 * @return ?IBackup - null if no such backup exists
	 */
	function get(int $id): ?IBackup;
	
	/**
	 * Returns all existing backups
	 * @return IBackup[]
	 */
	function list(): array;
	
	/**
	 * Deletes older backup files
	 */
	function purge(): bool;
} // interface IManager
