<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\Managers;

use Nette\IOException;
use Nette\Utils\FileSystem;


/**
 * Advanced backups manager using locks to protect ongoing backup creation
 */
class Locking extends Simple {
	protected function runJobs(string $baseName): array {
		$lockFile = $this->getDir().'/'.$baseName.'.lock';
		
		// acquire an exclusive lock
		$lock = fopen($lockFile, 'w+');
		if($lock === false || !flock($lock, LOCK_EX)){
			throw new IOException('Unable to acquire lock!');
		};
		
		try {
			$files = $this->runJobsLocked($baseName);
		}
		finally {
			// unlock
			flock($lock, LOCK_UN);
			fclose($lock);
			
			try {
				FileSystem::delete($lockFile);
			}
			catch(IOException $e){};
		};
		
		return $files;
	} // runJobs
	
	/**
	 * Runs jobs inside locked context
	 * @return string[]
	 */
	protected function runJobsLocked(string $baseName): array {
		return parent::runJobs($baseName);
	} // runJobsLocked
} // class Locking
