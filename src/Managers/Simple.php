<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups\Managers;

use InvalidArgumentException;
use Nette;
use Nette\IOException;
use Nette\Utils\DateTime;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;

use ElektroPotkan\Backups\Backup;
use ElektroPotkan\Backups\BackupFile;
use ElektroPotkan\Backups\IBackup;
use ElektroPotkan\Backups\IJob;
use ElektroPotkan\Backups\ILogger;
use ElektroPotkan\Backups\IManager;
use ElektroPotkan\Backups\IPurgeRule;


/**
 * Simple backups manager
 */
class Simple implements IManager {
	use Nette\SmartObject;
	
	
	/** @var string */
	private $dir;
	
	/** @var string */
	private $name;
	
	/** @var IPurgeRule */
	private $purgeRule;
	
	/** @var IJob[] */
	private $jobs = [];
	
	/** @var ?ILogger */
	protected $logger = null;
	
	
	/**
	 * Constructor
	 */
	public function __construct(string $dir, string $name, IPurgeRule $purgeRule){
		$this->dir = $dir;
		$this->name = $name;
		$this->purgeRule = $purgeRule;
	} // constructor
	
	/**
	 * Returns path to backups directory
	 */
	public function getDir(): string {
		return $this->dir;
	} // getDir
	
	/**
	 * Adds backup job
	 * @param string $name - unique name of the job, will be used as part of its backup file-name
	 */
	public function addJob(string $name, IJob $job): void {
		if($name === ''){
			throw new InvalidArgumentException('Job name cannot be empty!');
		};
		
		if(isset($this->jobs[$name])){
			throw new InvalidArgumentException('Job of given name already exists!');
		};
		
		$this->jobs[$name] = $job;
	} // addJob
	
	/**
	 * Creates new backup
	 * @return int - ID (timestamp) of created backup
	 */
	public function create(): int {
		if(count($this->jobs) < 1){
			throw new InvalidArgumentException('No job registered!');
		};
		
		// create backup ID (timestamp)
		$dt = new DateTime;
		$id = (int) $dt->format('U');
		
		// build base file-name
		$fn = [
			$dt->format('U_Y-m-d'),
			$this->name,
		];
		
		$fn = implode('_', $fn);
		
		// create backup files
		$this->log("Backups: Creating $id ($fn)...");
		$this->runJobs($fn);
		$this->log("Backups: Created $id.");
		
		return $id;
	} // create
	
	/**
	 * Runs registered backup jobs
	 * @param string $baseName - base of each file-name
	 * @return string[] - array of created file-names
	 */
	protected function runJobs(string $baseName): array {
		$files = [];
		
		foreach($this->jobs as $name => $job){
			$this->log("Backups: Creating job '$name'...");
			
			$fn = $this->formatJobFilename($baseName, $name, $job);
			$fp = $this->dir.'/'.$fn;
			
			$job->create($fp);
			
			$jobCreatedFile = is_file($fp);
			if($jobCreatedFile){
				$files[] = $fn;
			};
			
			$this->log("Backups: Created job '$name' - " . ($jobCreatedFile ? 'OK' : 'NO FILE') . '.');
		};
		
		return $files;
	} // runJobs
	
	/**
	 * Deletes backup of given ID
	 * @return bool
	 *   - true = OK
	 *   - false = ERROR
	 *   - null = not found
	 */
	public function delete(int $id): ?bool {
		$files = Finder::findFiles($id.'_*')->in($this->dir);
		
		if(count($files) < 1){
			return null;
		};
		
		$this->log("Backups: Deleting $id...", ILogger::DEBUG);
		
		$ret = true;
		foreach($files as $path => $file){
			$this->log("Backups: Deleting file '$path'...", ILogger::DEBUG);
			
			try {
				FileSystem::delete($path);
			}
			catch(IOException $e){
				$this->log($e, ILogger::ERROR);
				$ret = false;
			};
		};
		
		$this->log("Backups: Deleted $id - " . ($ret ? 'OK' : 'ERROR') . '.', ILogger::DEBUG);
		
		return $ret;
	} // delete
	
	/**
	 * Returns backup of given ID
	 * @return ?IBackup - null if no such backup exists
	 */
	public function get(int $id): ?IBackup {
		$files = Finder::findFiles($id.'_*')->in($this->dir);
		if(count($files) < 1){
			return null;
		};
		
		$bak = new Backup($id, new DateTime('@'.$id));
		
		foreach($files as $path => $file){
			$bak->addFile(new BackupFile($file));
		};
		
		return $bak;
	} // get
	
	/**
	 * Returns all existing backups
	 * @return IBackup[]
	 */
	public function list(): array {
		$files = Finder::findFiles('*')->exclude('.*')->in($this->dir);
		
		$baks = [];
		foreach($files as $path => $file){
			// parse ID
			$id = self::parseId($file->getFilename());
			if($id === null){
				continue;
			};
			
			if(!isset($baks[$id])){
				$baks[$id] = new Backup($id, new DateTime('@'.$id));
			};
			
			$baks[$id]->addFile(new BackupFile($file));
		};
		
		return $baks;
	} // list
	
	/**
	 * Deletes older backup files
	 */
	public function purge(): bool {
		$this->log('Backups: Purging...');
		
		$now = new DateTime;
		
		$files = Finder::findFiles('*')->exclude('.*')->in($this->dir);
		
		$ret = true;
		foreach($files as $path => $file){
			$fn = $file->getFilename();
			
			if(strlen($fn) < 20){
				continue;
			};
			
			// parse ID
			$id = self::parseId($fn);
			if($id === null){
				continue;
			};
			
			$dt = new DateTime('@'.$id);
			
			// decide
			if($this->purgeRule->keepOrPurge($dt, $now)){
				continue;
			};
			
			// delete file
			$this->log("Backups: Purging file '$path'...");
			
			try {
				FileSystem::delete($path);
			}
			catch(IOException $e){
				$this->log($e, ILogger::ERROR);
				$ret = false;
			};
		};
		
		$this->log('Backups: Purging finished - ' . ($ret ? 'OK' : 'ERROR') . '.');
		
		return $ret;
	} // purge
	
	/**
	 * Returns formatted file-name for given job
	 */
	protected function formatJobFilename(string $baseName, string $jobName, IJob $job): string {
		$ext = $job->getExtension();
		if($ext !== ''){
			$ext = '.' . $ext;
		};
		
		return $baseName . '_' . $jobName . $ext;
	} // formatJobFilename
	
	/**
	 * Parses given filename into ID (timestamp)
	 * @return ?int - ID when valid backup file-name, null when invalid
	 */
	public static function parseId(string $filename): ?int {
		$fnid = explode('_', $filename, 2);
		// no count check - we also accept timestamp-only input
		
		$id = intval($fnid[0]);
		if(strval($id) !== $fnid[0]){
			return null;
		};
		
		return $id;
	} // parseId
	
	public function setLogger(ILogger $logger): void {
		$this->logger = $logger;
	} // setLogger
	
	/**
	 * @param mixed $value
	 */
	protected function log($value, string $level = ILogger::INFO): void {
		if($this->logger !== null){
			$this->logger->log($value, $level);
		};
	} // log
} // class Simple
