<?php

declare(strict_types=1);

namespace ElektroPotkan\Backups;

use Nette;
use Tracy;


class TracyLogger implements ILogger {
	use Nette\SmartObject;
	
	
	private const LEVEL_MAP = [
		ILogger::CRITICAL => Tracy\ILogger::CRITICAL,
		ILogger::EXCEPTION => Tracy\ILogger::EXCEPTION,
		ILogger::ERROR => Tracy\ILogger::ERROR,
		ILogger::WARNING => Tracy\ILogger::WARNING,
		ILogger::INFO => Tracy\ILogger::INFO,
		ILogger::DEBUG => Tracy\ILogger::DEBUG,
	];
	
	
	/** @var Tracy\ILogger */
	private $logger;
	
	
	public function __construct(Tracy\ILogger $logger){
		$this->logger = $logger;
	} // constructor
	
	/**
	 * @param mixed $value
	 */
	public function log($value, string $level = self::INFO): void {
		$this->logger->log($value, self::LEVEL_MAP[$level] ?? Tracy\ILogger::ERROR);
	} // log
} // class TracyLogger
