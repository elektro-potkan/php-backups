<?php

declare(strict_types=1);


/* Load Composer-maintained packages */
require_once dirname(__DIR__).'/vendor/autoload.php';


/* Tester setup */
Tester\Environment::setup();
