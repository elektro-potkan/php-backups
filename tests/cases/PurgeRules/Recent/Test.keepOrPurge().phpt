<?php

declare(strict_types=1);

namespace Tests\Cases\PurgeRules\Recent;

require __DIR__ . '/../../../bootstrap.php';

use Generator;
use Nette\Utils\DateTime;
use Tester;
use Tester\Assert;

use ElektroPotkan\Backups\PurgeRules\Recent as Rule;


class Test_keepOrPurge extends Tester\TestCase {
	public static function genArgsKeep(): Generator {
		$times = [
			'2020-08-15 13:05:56',
			'2020-08-15 13:05:57',
			'2020-08-18 18:19:56',
			'2020-08-16 03:05:56',
			'2020-08-17 03:05:56',
			'2020-08-19 03:05:56',
			'2020-08-20 03:05:56',
			'2020-08-21 03:05:56',
			'2020-08-21 13:05:56',
		];
		
		foreach($times as $t){
			yield [$t, '2020-08-22 13:05:56', 60*24*7];
		};
		
		$times = [
			'2024-07-09 08:24:37',
			'2024-07-09 08:24:38',
			'2024-07-10 03:05:56',
			'2024-07-13 03:05:56',
			'2024-07-12 03:05:56',
		];
		
		foreach($times as $t){
			yield [$t, '2024-07-13 08:24:37', 60*24*4];
		};
	} // genArgsKeep
	
	/**
	 * @dataProvider genArgsKeep
	 */
	public function testKeep(string $dt, string $now, int $mins): void {
		$dt = DateTime::createFromFormat('Y-m-d H:i:s', $dt, 'UTC');
		$now = DateTime::createFromFormat('Y-m-d H:i:s', $now, 'UTC');
		
		Assert::true((new Rule($mins))->keepOrPurge($dt, $now));
	} // testKeep
	
	public static function genArgsPurge(): Generator {
		$times = [
			'2020-06-24 17:26:00',
			'0000-02-01 00:00:00',
			'2019-04-05 18:19:56',
			'2019-10-28 17:57:21',
			'2020-08-16 13:05:55',
			'2020-08-15 13:05:56',
			'2020-08-14 13:05:56',
		];
		
		foreach($times as $t){
			yield [$t, '2020-08-22 13:05:56', 60*24*6];
		};
		
		$times = [
			'2020-06-24 17:26:00',
			'0000-02-01 00:00:00',
			'2019-04-05 18:19:56',
			'2019-10-28 17:57:21',
			'2026-03-01 17:57:21',
			'2026-03-01 23:59:59',
			'2019-03-15 06:40:21',
			'2026-02-15 17:57:21',
			'2026-01-28 17:57:21',
			'2026-03-02 06:47:30',
			'2026-03-01 06:47:31',
			'2026-03-02 06:47:29',
		];
		
		foreach($times as $t){
			yield [$t, '2026-03-15 06:47:31', 60*24*13];
		};
	} // genArgsPurge
	
	/**
	 * @dataProvider genArgsPurge
	 */
	public function testPurge(string $dt, string $now, int $mins): void {
		$dt = DateTime::createFromFormat('Y-m-d H:i:s', $dt, 'UTC');
		$now = DateTime::createFromFormat('Y-m-d H:i:s', $now, 'UTC');
		
		Assert::false((new Rule($mins))->keepOrPurge($dt, $now));
	} // testPurge
} // class Test_keepOrPurge


(new Test_keepOrPurge)->run();
