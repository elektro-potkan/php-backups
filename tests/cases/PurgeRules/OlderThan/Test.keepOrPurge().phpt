<?php

declare(strict_types=1);

namespace Tests\Cases\PurgeRules\OlderThan;

require __DIR__ . '/../../../bootstrap.php';

use Generator;
use Nette\Utils\DateTime;
use Tester;
use Tester\Assert;

use ElektroPotkan\Backups\PurgeRules\OlderThan as Rule;


class Test_keepOrPurge extends Tester\TestCase {
	public static function genArgsKeep(): Generator {
		$times = [
			'2019-07-22 13:05:56',
			'2019-07-22 13:05:57',
			'2020-04-01 18:19:56',
			'2020-01-01 17:57:21',
			'2020-04-01 18:19:56',
			'2020-06-01 08:29:56',
			'2020-02-01 20:49:56',
			'2020-08-15 03:05:56',
			'2020-08-17 03:05:56',
			'2019-08-19 03:05:56',
			'2020-08-20 03:05:56',
			'2019-08-21 03:05:56',
			'2020-08-21 13:05:56',
		];
		
		foreach($times as $t){
			yield [$t, '2019-07-22 13:05:56'];
		};
		
		$times = [
			'2024-07-13 08:24:37',
			'2024-07-13 08:24:38',
			'2029-01-01 17:26:00',
			'2026-01-01 00:00:00',
			'2025-01-01 18:19:56',
			'2027-01-01 17:57:21',
			'2024-08-01 18:19:56',
			'2024-09-01 08:29:56',
			'2024-07-16 20:49:56',
			'2024-07-18 03:05:56',
			'2024-08-07 03:05:56',
			'2024-07-22 03:05:56',
		];
		
		foreach($times as $t){
			yield [$t, '2024-07-13 08:24:37'];
		};
	} // genArgsKeep
	
	/**
	 * @dataProvider genArgsKeep
	 */
	public function testKeep(string $dt, string $time): void {
		$time = DateTime::createFromFormat('Y-m-d H:i:s', $time, 'UTC');
		$dt = DateTime::createFromFormat('Y-m-d H:i:s', $dt, 'UTC');
		$now = new DateTime;
		
		Assert::true((new Rule($time))->keepOrPurge($dt, $now));
	} // testKeep
	
	public static function genArgsPurge(): Generator {
		$times = [
			'2020-08-22 13:05:55',
			'2020-06-24 17:26:00',
			'0000-02-01 00:00:00',
			'2020-08-21 13:05:56',
			'2019-04-05 18:19:56',
			'2019-10-28 17:57:21',
			'2020-06-28 17:57:21',
		];
		
		foreach($times as $t){
			yield [$t, '2020-08-22 13:05:56'];
		};
		
		$times = [
			'2020-06-24 17:26:00',
			'0000-02-01 00:00:00',
			'2019-04-05 18:19:56',
			'2019-10-28 17:57:21',
			'2026-03-07 17:57:21',
			'2026-03-07 23:59:59',
			'2019-03-15 06:40:21',
			'2026-02-15 17:57:21',
			'2026-01-28 17:57:21',
			'2026-03-15 06:47:30',
		];
		
		foreach($times as $t){
			yield [$t, '2026-03-15 06:47:31'];
		};
	} // genArgsPurge
	
	/**
	 * @dataProvider genArgsPurge
	 */
	public function testPurge(string $dt, string $time): void {
		$time = DateTime::createFromFormat('Y-m-d H:i:s', $time, 'UTC');
		$dt = DateTime::createFromFormat('Y-m-d H:i:s', $dt, 'UTC');
		$now = new DateTime;
		
		Assert::false((new Rule($time))->keepOrPurge($dt, $now));
	} // testPurge
} // class Test_keepOrPurge


(new Test_keepOrPurge)->run();
