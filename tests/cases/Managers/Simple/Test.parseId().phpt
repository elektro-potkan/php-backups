<?php

declare(strict_types=1);

namespace Tests\Cases\Managers\Simple;

require __DIR__ . '/../../../bootstrap.php';

use Generator;
use Tester;
use Tester\Assert;

use ElektroPotkan\Backups\Managers\Simple as Manager;


class Test_parseId extends Tester\TestCase {
	public static function genArgsValid(): array {
		return [
			['2021_06-24_17-26-00', 2021],
			['0_01-01_00-00-00', 0],
			['1475264_2025-04-05_18-19-56', 1475264],
			['16427485_2019-10-28_17-57-21_FuelApp_v1.47.2_nejaka-databaze.sql.gz', 16427485],
			['1629957219_2021_08_26_14.1.2_fuel_backup.tar', 1629957219],
			['1629856847_2021_08_25_FuelApp_v14.2.1_bak_.tar', 1629856847],
			['1629856847', 1629856847],
			['1629', 1629],
		];
	} // genArgsValid
	
	/**
	 * @dataProvider genArgsValid
	 */
	public function testValid(string $filename, int $id): void {
		Assert::same($id, Manager::parseId($filename));
	} // testValid
	
	public static function genArgsInvalid(): Generator {
		$values = [
			'',
			'   		 ',
			'G3DRH15',
			'95-12-03_14-45-32',
			'0000_01-01_00-00-00',
			'2021-06-24_17-24-60',
			'2021-02-30_17-24-00',
			'2021-04-31_17-24-00',
			'1997-13-04_18-19-20',
			'1997-00-04_18-19-20',
			'1997-11-00_18-19-20',
			'1997-11-34_18-19-20',
			'1997-11-04_28-19-20',
			'2021-06-24_17-64-00',
			'2021-06-24_17-24-6',
			'g2025-04-05_18-19-56',
			'/gd/2025-04-05_18-19-56',
			'2025-04-05_18-19-56.zip',
			'../config/2025-04-05_18-19-56',
			'./2025-04-05_18-19-56',
			'-782025-04-05_18-19-56_47',
			'/3433484_154_neco',
			'./3433484_154_neco',
			'../3433484_154_neco',
			'../app/3433484_154_neco',
			'1629856847.tar',
		];
		
		foreach($values as $val){
			yield [$val];
		};
	} // genArgsInvalid
	
	/**
	 * @dataProvider genArgsInvalid
	 */
	public function testInvalid(string $filename): void {
		Assert::null(Manager::parseId($filename));
	} // testInvalid
} // class Test_parseId


(new Test_parseId)->run();
