<?php

declare(strict_types=1);

namespace Tests\Cases\Managers\Simple;

require __DIR__ . '/../../../bootstrap.php';

use DateTimeInterface;
use Generator;
use InvalidArgumentException;
use Tester;
use Tester\Assert;

use ElektroPotkan\Backups\Managers\Simple as Manager;
use ElektroPotkan\Backups\PurgeRules\AlwaysKeep;

use Tests\Mocks\Job;


class Test_addJob extends Tester\TestCase {
	/**
	 * @param string[] $excludeNames
	 * @return string[]
	 */
	public static function genFileName(array $excludeNames = []): array {
		$charsExt = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		$charsName = $charsExt . '_-';
		
		$maxExt = strlen($charsExt) - 1;
		$maxName = strlen($charsName) - 1;
		
		$lenExt = rand(0, 6);
		$lenName = rand(1, 20);
		
		$ext = '';
		for($i = 0; $i < $lenExt; $i++){
			$ext .= $charsExt[rand(0, $maxExt)];
		};
		
		do {
			$name = '';
			for($i = 0; $i < $lenName; $i++){
				$name .= $charsName[rand(0, $maxName)];
			};
		}
		while(in_array($name, $excludeNames, true));
		
		return [$name, $ext];
	} // genFileName
	
	/**
	 * @param string[] $excludeNames
	 * @return [string, Job]
	 */
	public static function genJob(array $excludeNames = []): array {
		list($name, $ext) = self::genFileName($excludeNames);
		return [$name, new Job($ext)];
	} // genJob
	
	/**
	 * @param string[] $excludeNames
	 */
	public static function genJobs(int $count, array $excludeNames = []): Generator {
		for($i = 0; $i < $count; $i++){
			list($name, $job) = self::genJob($excludeNames);
			yield $name => $job;
			$excludeNames[] = $name;
		};
	} // genJobs
	
	public function testDuplicates(): void {
		for($i = 0; $i < 10; $i++){
			$manager = new Manager('/tmp', 'test-name', new AlwaysKeep);
			
			$usedNames = [];
			
			for($j = 0; $j < 3; $j++){
				foreach(self::genJobs(rand(5, 10), $usedNames) as $name => $job){
					$usedNames[] = $name;
					$manager->addJob($name, $job);
				};
				
				foreach($usedNames as $name){
					$ext = self::genFileName()[1];
					Assert::exception(
						function() use($manager, $name, $ext): void {
							$manager->addJob($name, new Job($ext));
						},
						InvalidArgumentException::class,
						'Job of given name already exists!'
					);
				};
			};
		};
	} // testDuplicates
} // class Test_addJob


(new Test_addJob)->run();
